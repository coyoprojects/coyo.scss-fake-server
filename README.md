# COYO SCSS fake server

This node server simply delivers

- a single (JSON) file or
- multiple files differentiated via the POST request body [default]

## Build the docker image

docker build -t mindsmash/coyo-scss-fake:<version> .

## Run the docker image
docker images
docker run -p 3030:3030 -d mindsmash/coyo-scss-fake:<version>

## See stats of the server
docker ps
docker logs <container id>

## Push the docker image
docker push mindsmash/coyo-scss-fake:<version>
