/**
 * COYO SCSS fake server
 * 
 * (c) Copyright 2018 Denis Meyer, COYO GmbH. All rights reserved.
 */

const MODULE_NAME = 'config::server';

const dev = false;

// Leave server.address blank to expose server to the public.
// Otherwise set server.address to '127.0.0.1'.
let server = {
    address: '',
    port: 3030
};

if (dev) {
    server.address = '127.0.0.1';
}

module.exports = server;