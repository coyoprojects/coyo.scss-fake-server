/**
 * COYO SCSS fake server
 * 
 * (c) Copyright 2018 Denis Meyer, COYO GmbH. All rights reserved.
 */

const MODULE_NAME = 'config::options';

let options = {
    debug: true,
    handleMultipleFiles: true,
    customThemeKey: 'eonThemeId'
};

module.exports = options;