/**
 * COYO SCSS fake server
 * 
 * (c) Copyright 2018 Denis Meyer, COYO GmbH. All rights reserved.
 */

const MODULE_NAME = 'config::files';

// 'default' should be present
// format: 'themeName': '/path/to/file'
module.exports = {
    file: 'files/default.json',
    defaultFileName: 'default',
    files: {
        'default': 'files/default.json',
        'avacon': 'files/avacon.json',
        'bayernwerk': 'files/bayernwerk.json',
        'edialognetz': 'files/edialognetz.json',
        'edis': 'files/edis.json',
        'ekundenservicenetz': 'files/ekundenservicenetz.json',
        'hansewerk': 'files/hansewerk.json',
        'preussenelektra': 'files/preussenelektra.json'
    }
};