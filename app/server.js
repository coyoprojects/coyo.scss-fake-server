/**
 * COYO SCSS fake server
 * 
 * (c) Copyright 2018 Denis Meyer, COYO GmbH. All rights reserved.
 */

const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');

const logger = require('./utils/logger');
const configOptions = require('./config/options');
const utilsFiles = require('./utils/files');
const configServer = require('./config/server');
const configFiles = require('./config/files');
const utilsThemeExtractor = require('./utils/themeExtractor');

const MODULE_NAME = 'server';

if (!configOptions.handleMultipleFiles) {
    logger.log(MODULE_NAME, 'Loading file...', configFiles.file);
    let fileContent;
    utilsFiles.readFile(configFiles.file, function (err, contents) {
        if (err) {
            logger.log(MODULE_NAME, 'Errors while loading file:', err);
            return;
        }

        fileContent = contents;
        logger.log(MODULE_NAME, 'Successfully loaded file.');
        logger.log(MODULE_NAME, 'Registering middleware...');

        const app = express();

        // load middleware functions at a path for all request methods
        app.all('*', (req, res) => {
            logger.logRequest(MODULE_NAME, req, new Date());
            res.setHeader('content-type', 'application/json');
            res.send(fileContent);
        });

        logger.log(MODULE_NAME, 'Starting server...');

        const server = app.listen(configServer.port, configServer.address, () => {
            logger.log(MODULE_NAME, 'Server started on', '"' + configServer.address + ':' + configServer.port + '"');
        });
    });
} else {
    logger.log(MODULE_NAME, 'Loading files...', configFiles.files);
    utilsFiles.readFiles(configFiles.files, function (errorOccurred, errors, data) {
        if (errorOccurred || data.length <= 0) {
            logger.log(MODULE_NAME, 'Errors while loading files:', errors);
            return;
        }
        const fileData = data;
        let defaultFile;
        if (fileData.hasOwnProperty(configFiles.defaultFileName)) {
            logger.log(MODULE_NAME, 'Found default file', '"' + configFiles.defaultFileName + '".');
            defaultFile = fileData[configFiles.defaultFileName];
        } else {
            logger.log(MODULE_NAME, 'Could not find default file, taking first of files list.');
            for (let fKey in fileData) {
                defaultFile = fileData[fKey];
                break;
            }
        }

        logger.log(MODULE_NAME, 'Successfully loaded files.');
        logger.log(MODULE_NAME, 'Registering middleware...');

        const app = express();

        // load middleware functions at a path for all request methods
        app.use(bodyParser.json({
            type: 'application/json'
        }));
        app.all('*', (req, res) => {
            const reqDate = new Date();
            logger.logRequest(MODULE_NAME, req, reqDate);
            res.setHeader('content-type', 'application/json');
            if (req.method === 'POST') {
                themeId = utilsThemeExtractor.getThemeId(req);
                if (themeId) {
                    logger.log(MODULE_NAME, 'Delivering theme with ID', '"' + themeId + '".', reqDate);
                    res.send(fileData[themeId]);
                } else {
                    logger.log(MODULE_NAME, 'Could not find valid theme ID in request.', '', reqDate);
                    res.send(defaultFile);
                }
            } else {
                res.send(defaultFile);
            }
        });

        logger.log(MODULE_NAME, 'Starting server...');

        const server = app.listen(configServer.port, configServer.address, () => {
            logger.log(MODULE_NAME, 'Server started on', '"' + configServer.address + ':' + configServer.port + '"');
        });
    });
}