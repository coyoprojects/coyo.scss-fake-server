/**
 * COYO SCSS fake server
 * 
 * (c) Copyright 2018 Denis Meyer, COYO GmbH. All rights reserved.
 */

const fs = require('fs');
const MODULE_NAME = 'utils::files';

function readFile(file, cb) {
    fs.readFile(file, 'utf8', cb);
}

function readFiles(files, cb) {
    let errorOccurred = false;
    let errors = {};
    let result = {};
    let nrOfReadFiles = Object.keys(files).length;
    for (let fKey in files) {
        fs.readFile(files[fKey], 'utf8', function (err, data) {
            errors[fKey] = err;
            result[fKey] = data;
            --nrOfReadFiles;
            if (err) {
                errorOccurred = true;
            }
            if (nrOfReadFiles <= 0) {
                if (cb) {
                    cb(errorOccurred, errors, result);
                } else {
                    return;
                }
            }
        });
    }
}

module.exports = {

    /**
     * Reads a single files and calls a callback function afterwards.
     * 
     * @param {array} files The file
     * @param {function} cb The callback function
     */
    readFile: readFile,

    /**
     * Reads multiple files and calls a callback function afterwards.
     * 
     * @param {array} files The files
     * @param {function} cb The callback function
     */
    readFiles: readFiles
};