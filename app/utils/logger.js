/**
 * COYO SCSS fake server
 * 
 * (c) Copyright 2018 Denis Meyer, COYO GmbH. All rights reserved.
 */

const MODULE_NAME = 'utils::logger';

function log(moduleName, str, obj, reqDate) {
    if (obj) {
        console.log('[' + moduleName + ']', '[' + (reqDate ? reqDate : new Date()) + ']', str, obj);
    } else {
        console.log('[' + moduleName + ']', '[' + (reqDate ? reqDate : new Date()) + ']', str);
    }
}

function logRequest(moduleName, req, reqDate) {
    log(moduleName, req.method, req.url);
}

module.exports = {

    /**
     * Logs things.
     * 
     * @param {string} moduleName The module name
     * @param {string} str The string to log
     * @param {object} obj An optional object to be logged
     * @param {object} reqDate An optional date to be logged
     */
    log: log,

    /**
     * Logs a request with more information.
     * 
     * @param {string} moduleName The module name
     * @param {object} req The request to be logged
     * @param {object} reqDate An optional date to be logged
     */
    logRequest: logRequest
};