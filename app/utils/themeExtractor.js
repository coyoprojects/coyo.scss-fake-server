/**
 * COYO SCSS fake server
 * 
 * (c) Copyright 2018 Denis Meyer, COYO GmbH. All rights reserved.
 */

const configOptions = require('../config/options');
const configFiles = require('../config/files');

const MODULE_NAME = 'config::themeExtractor';

function getThemeId(req) {
    let result = null;
    if (req && req.body && req.body.variables) {
        for (let varKeys in req.body.variables) {
            if (req.body.variables[varKeys].key && req.body.variables[varKeys].value) {
                const isCustomTheme = req.body.variables[varKeys].key === configOptions.customThemeKey;
                const hasThemeContent = req.body.variables[varKeys].value && configFiles.files.hasOwnProperty(req.body.variables[varKeys].value);
                if (isCustomTheme && hasThemeContent) {
                    result = req.body.variables[varKeys].value;
                    break;
                }
            }
        }
    }

    return result;
}

module.exports = {

    /**
     * Extracts the theme ID. If not found, returns null.
     * 
     * @param {object} req The request to extract the theme ID from
     */
    getThemeId: getThemeId
};